// using the Object.keys => creates an array that contains the properties of an
// object
let fruits = {
  mangoes: 50,
  jackfruits: 20,
  oranges: 50,
  guavas: 5
}

let keys = Object.keys(fruits);

console.log('Using the Object.keys method')
console.log(keys);
console.log('============================')

// using the Object.values => creates an array that contains the values of every
// property in an object
let differentFruits = {
  berries: 50,
  cucamber: 20,
  grapes: 50,
  apple: 5
}

let values = Object.values(differentFruits);

console.log('Using the Object.values method')
console.log(values);
console.log('============================')

// using the Object.entries => creates an array of arrays that contains the both
// the values and properties in an object
let moreFruits = {
  mangoes: 50,
  jackfruits: 20,
  oranges: 50,
  guavas: 5,
  berries: 50,
  cucamber: 20,
  grapes: 50,
  apple: 5
}

let entries = Object.entries(moreFruits);

console.log('Using the Object.entries method')
console.log(entries);
console.log('============================')

// Looping through the array

// Looping through the array from the Object.keys

for (var key in keys) {
  console.log(keys);
}

// Looping through the array from the Object.entries

for (let [moreFruits, count] of entries) {
  console.log(`There are ${count} ${moreFruits}s`);
}
